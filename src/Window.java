import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * The class is responsible for the interaction of the user
 * with the application window.
 */

public class Window extends JFrame {

    /**
     * The main method of application. Launches the application
     * window in a new thread.
     * @param args
     *        parameters that can be passed at startup from the
     *        command line.
     */

    public static void main(String[] args) {
        SwingUtilities.invokeLater(Window::new);
    }

    /**
     * The method describes and displays the application window
     * and its content and the response of the window and its
     * elements to user actions.
     */

    private Window(){
        super("Image Defend");
        setLayout(null);
        JTextArea log = new JTextArea();
        JTextField fieldInput = new JTextField();
        JScrollPane scrollPane = new JScrollPane(log);
        Menu menu = new Menu(log);
        JButton button = new JButton("Ввод");
        setSize(640, 480);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        log.setEditable(false);
        scrollPane.setBounds(0, 0, 635, 428);
        fieldInput.setBounds(0, 428, 535, 25);
        fieldInput.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == 10 && fieldInput.getText().length() > 0) {
                    menu.setReadedString(fieldInput.getText());
                    fieldInput.setText(null);

                }
            }
        });
        button.setBounds(535, 428, 100, 25);
        button.addActionListener(e -> {
            if (fieldInput.getText().length() > 0) {
                menu.setReadedString(fieldInput.getText());
                fieldInput.setText(null);
            }
        });
        add(scrollPane);
        add(fieldInput);
        add(button);
        setVisible(true);
        menu.start();
    }
}