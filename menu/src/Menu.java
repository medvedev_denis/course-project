import javax.swing.*;
import java.io.*;
import java.security.NoSuchAlgorithmException;

/**
 * A class for interacting with the user. Displays
 * the necessary information on the screen and accepts the input data.
 */

public class Menu extends Thread{

    private JTextArea log;
    private String readedString = "";

    /**
     * The constructor of the Menu class.
     * @param textArea
     *        A link to the text area for displaying messages
     *        for interaction with the user.
     */

    Menu(JTextArea textArea) {
        log = textArea;
    }

    /**
     * The method starts the menu in a new thread.
     */

    public void run() {
        this.menu();
    }

    /**
     * Method is the primary means of interaction with the user.
     */

    private void menu() {
        boolean exitFromTheProgram = false;
        while (!exitFromTheProgram) {
            ImageEncryptor encryptor = null;
            ImageDecryptor decryptor;
            String stringOuterChoise;
            File file;
            int choise;
            boolean correctOuterData = false;
            printText("Главное меню проекта по дешифрации изображений.");
            printText("Для дальнейшей работы проекта нажмите:\n    `1` - Запуск процесса шифрования\n    " +
                    "`2` - Запуск процесса дешифрования\n    `3` - Выход из программы");
            while (!correctOuterData) {
                if (isInt(stringOuterChoise = readString())) {
                    choise = Integer.parseInt(stringOuterChoise);
                    switch (choise) {
                        case 1:
                            printText("Введите путь к файлу");
                            while (true) {
                                file = new File(readString());
                                if (file.isFile()) {
                                    try {
                                        encryptor = new ImageEncryptor(file, log);
                                    } catch (NoSuchAlgorithmException e) {
                                        e.printStackTrace();
                                    }
                                    printText("Вы действительно хотите зашифровать " + file.getName() + "?");
                                    printText("    `1` - Подтвердить шифрование изображения\n    `2` - Отмена шифрования");
                                    String stringInnerChoise;
                                    int innerChoise;
                                    boolean correctInnerData = false;
                                    while (!correctInnerData) {
                                        if (isInt(stringInnerChoise = readString())) {
                                            innerChoise = Integer.parseInt(stringInnerChoise);
                                            switch (innerChoise) {
                                                case 1:
                                                    encryptor.encrypt();
                                                    correctInnerData = true;
                                                    break;
                                                case 2:
                                                    correctInnerData = true;
                                                    break;
                                                default:
                                                    printText("Такого пункта меню не существует! ");
                                                    break;
                                            }
                                        }
                                    }
                                    correctOuterData = true;
                                    break;
                                } else {
                                    printText("Введен неверный путь! Попробуйте ещё раз.");
                                }
                            }
                            break;
                        case 2:
                            printText("Введите путь к файлу");
                            while (true) {
                                file = new File(readString());
                                if (file.isFile()) {
                                    decryptor = new ImageDecryptor(file, log);
                                    printText("Вы действительно хотите дешифровать " + file.getName() + "?");
                                    printText("    `1` - Подтвердить дешифрование изображения\n    `2` - Отмена дешифрования");
                                    String stringInnerChoise;
                                    int innerChoise;
                                    boolean correctData = false;
                                    while (!correctData) {
                                        if (isInt(stringInnerChoise = readString())) {
                                            innerChoise = Integer.parseInt(stringInnerChoise);
                                            switch (innerChoise) {
                                                case 1:
                                                    decryptor.decrypt();
                                                    correctData = true;
                                                    break;
                                                case 2:
                                                    correctData = true;
                                                    break;
                                                default:
                                                    printText("Такого пункта меню не существует! ");
                                                    break;
                                            }
                                        }
                                    }
                                    correctOuterData = true;
                                    break;
                                } else {
                                    printText("Введен неверный путь! Попробуйте ещё раз.");
                                }
                            }
                            break;
                        case 3:
                            correctOuterData = true;
                            exitFromTheProgram = true;
                            printText("Выполняется выход из программы!");
                            System.exit(0);
                        default:
                            printText("Такого пункта меню не существует! ");
                            break;
                    }
                }
            }
        }
    }

    /**
     * Checks whether the received string is a number.
     * @param string checked string.
     * @return result of checking.
     */

    private boolean isInt(String string) {
        try {
             Integer.parseInt(string);
        } catch (Exception e) {
            printText("Введены некорректные данные! Попробуйте ещё раз.");
            return false;
        }
        return true;
    }

    /**
     * Prints the string to be sent to the text area.
     * @param s output string.
     */

    private void printText(String s) {
        log.append(s + "\n");
    }

    /**
     * Waits for text to be entered in the text box.
     * @return entered text.
     */

    private String readString() {
        String returnString;
        while (readedString == null || readedString.equals("")) {
            Thread.yield();
        }
        returnString = readedString;
        readedString = "";
        printText(returnString);
        return returnString;
    }

    /**
     * Allows you to transfer text entered by the user text.
     * @param readedString the string read from the text field.
     */

    void setReadedString(String readedString) {
        this.readedString = readedString;
    }
}