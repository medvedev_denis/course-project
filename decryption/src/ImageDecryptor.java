import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

/**
 * A class is an implementation of an image decoder using
 * standard Java language tools.
 */

class ImageDecryptor {

    private File encryptionFileWithKey;
    private JTextArea log;

    /**
     * The constructor of the ImageDecryptor class.
     * @param file received for decryption.
     * @param textArea
     *        A reference to the text area for outputting a
     *        message with the decryption result.
     */

    ImageDecryptor(File file, JTextArea textArea) {
        log = textArea;
        encryptionFileWithKey = file;
    }

    /**
     * The method decrypts the resulting file and outputs the decryption result.
     */

    void decrypt() {
        try {
            String key = fileSharing();
            File encryptionFile = encryptionFileWithKey;
            encryptionFileWithKey = null;
            SecretKey decryptionKey = new SecretKeySpec(Base64.getDecoder().decode(key), 0, Base64.getDecoder().decode(key).length, "DES");
            byte[] encryptionByteImage = Files.readAllBytes(Paths.get(encryptionFile.getPath()));
            Cipher decryptor = Cipher.getInstance("DES");
            decryptor.init(Cipher.DECRYPT_MODE, decryptionKey);
            byte[] decryptionByteImage =  decryptor.doFinal(encryptionByteImage);
            DataOutputStream dos = new DataOutputStream(new FileOutputStream(encryptionFile.getName().substring(0, encryptionFile.getName().
                    lastIndexOf(".")) + " decrypted" + encryptionFile.getName().substring(encryptionFile.getName().lastIndexOf("."))));
            dos.write(decryptionByteImage);
            dos.flush();
            dos.close();
            log.append("Файл успешно дешифрован!\n");
        } catch (Exception e) {
            log.append("При дешифрации файла произошла ошибка. Возможно файл был поврежден!");
        }
    }

    /**
     * The method reads the encryption key and deletes it from the file.
     * @return string containing the encryption key
     * @throws Exception
     *         If there is no file or an I/O error occurs
     */

    private String fileSharing() throws Exception {
        RandomAccessFile keyDeleter = new RandomAccessFile(encryptionFileWithKey, "rw");
        String key;
        keyDeleter.seek(keyDeleter.length() - 12);
        key = keyDeleter.readLine();
        long length = keyDeleter.length() - 10;
        keyDeleter.seek(length);
        byte b = 0;
        while (b != 10) {
            b = keyDeleter.readByte();
            length -= 1;
            keyDeleter.seek(length);
        }
        keyDeleter.setLength(length);
        return key;
    }
}
