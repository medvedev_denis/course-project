import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.swing.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * A class is an implementation of an image encoder using
 * standard Java language tools.
 */

class ImageEncryptor {

    private File fileForEncryption;
    private SecretKey encryptionKey;
    private JTextArea log;

    /**
     * The constructor of the ImageEncryptor class.
     * @param file received for encryption.
     * @param textArea
     *        A reference to the text area for outputting a
     *        message with the decryption result.
     * @throws NoSuchAlgorithmException
     *         If you use an algorithm that is not available.
     */

    ImageEncryptor(File file, JTextArea textArea) throws NoSuchAlgorithmException {
        log = textArea;
        fileForEncryption = file;
        encryptionKey = KeyGenerator.getInstance("DES").generateKey();
    }

    /**
     * The method encrypts the received file and displays the result of the encryption.
     */

    void encrypt() {
        try {
            byte[] readByteImage = Files.readAllBytes(Paths.get(fileForEncryption.getPath()));
            Cipher encryptor = Cipher.getInstance("DES");
            encryptor.init(Cipher.ENCRYPT_MODE, encryptionKey);
            byte[] encryptionByteImage = encryptor.doFinal(readByteImage);
            String resultFileName = fileForEncryption.getName().substring(0, fileForEncryption.getName().
                    lastIndexOf(".")) + " encrypted" + fileForEncryption.getName().substring(fileForEncryption.getName().lastIndexOf("."));
            DataOutputStream dos = new DataOutputStream(new FileOutputStream(resultFileName));
            dos.write(encryptionByteImage);
            dos.flush();
            dos.close();
            BufferedWriter bw = new BufferedWriter(new FileWriter(resultFileName, true));
            bw.newLine();
            bw.write(Base64.getEncoder().encodeToString(encryptionKey.getEncoded()));
            bw.flush();
            bw.close();
            log.append("Файл успешно зашифрован!\n");
        }catch (Exception e) {
            log.append("Ошибка при шифрации файла!");
        }
    }
}
